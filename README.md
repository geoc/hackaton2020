<div align="center">

[<img src="https://www.inrae.fr/themes/custom/inrae_socle/logo.svg" height="140">](https://www.inrae.fr/)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
[<img src="https://ist.inrae.fr/wp-content/uploads/sites/21/2020/02/DipSO_logo-gradient-rvb-scaled.jpg" height="80">](https://ist.inrae.fr/list-a-inrae/dipso/)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
[<img src="https://www.mnhn.fr/sites/mnhn.fr/files/museum-national-d-histoire-naturelle_2.png" height="100">](https://www.mnhn.fr/)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
[<img src="https://www.france-bioinformatique.fr/wp-content/uploads/logo-ifb_small.png" height="100">](https://www.france-bioinformatique.fr/)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
[<img src="https://www.cnrs.fr/themes/custom/cnrs/logo.svg" height="100">](https://www.cnrs.fr/)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
[<img src="https://galaxyproject.eu/assets/media/tiaas-logo.png" height="50">](https://galaxyproject.eu/tiaas)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
[<img src="https://galaxyproject.org/images/galaxy-logos/galaxy_project_logo.png" height="70">](https://galaxyproject.org/)

# [Galaxy](https://galaxyproject.org/) [Interactive Tools](https://docs.galaxyproject.org/en/master/admin/special_topics/interactivetools.html) Hackathon du 7 au 10 décembre 2020

</div>

Un Hackathon pour partager des compétences en terme du développement logiciel et d'administration système des [Interactive Tools](https://docs.galaxyproject.org/en/master/admin/special_topics/interactivetools.html) de [Galaxy](https://galaxyproject.org/).

## Séquences, dates & horaires

|   Date               |  Horaires      |  Animateurs    | Titre                    |  Supports et ressources  |
| -------------------- | -------------- | -------------- | ------------------------ | ------------------------ |
| lundi 7 décembre     |  14:00 - 17:00 | Yvan & Patrick | Lancement & Introduction | [Galaxy Interactive Tools](https://training.galaxyproject.org/training-material/topics/admin/tutorials/interactive-tools/slides.html#1) |
| Mardi 8 décembre|  9:00 - 17:30  | Anthony & Estelle | Admin - Configurer Galaxy | [Galaxy Installation with Ansible](https://galaxyproject.github.io/training-material/topics/admin/tutorials/ansible-galaxy/tutorial.html) & [Galaxy Interactive Tools](https://galaxyproject.github.io/training-material/topics/admin/tutorials/interactive-tools/tutorial.html) |
| Mercredi 9 décembre & jeudi 10 décembre|  9:00 - 13:00 & 9:00 - 13:00  | Franck, Yvan , Lain, Romain, Coline, Elie | Développement d'IT pour Galaxy | --- |

## Animateurs

Ancelet Estelle - INRAE  
Arnaud Elie - MNHN  
Boulet Jean-Claude - INRAE  
Bretaudeau Anthony - INRAE - Membre IFB  
Chabrier Patrick - INRAE  
Dallet Romain - CNRS - Membre IFB  
Giacomoni Franck - INRAE  
Latrille Éric - INRAE  
Le Bras Yvan - MNHN  
Pavot Lain - INRAE  
Rossard Virginie - INRAE  
Royaux Coline - MNHN

## Programme et prérequis

Le nouveau système des interactives tools du gestionnaire de workflow galaxy permet de d'ajouter dans sa boite à outils des applications web. Les utilisateurs peuvent ainsi bénéficier d'interface graphiques dynamiques comme par exemple [Rshiny](https://shiny.rstudio.com), [RStudio](https://rstudio.com/) ou [jupyter](https://jupyter.org), ce qui constitue un progrès très important en terme d'ergonomie.

Du point de vue de l'architecture logicielle, les `interactives tools` reposent sur une pile de technologies informatiques plus complexe que les tools classiques. Et comme il s'agit de développements récents, la documentation sur le sujet est encore lacunaire.

Afin de nous approprier ce nouveau système de façon collective, nous proposons un ensemble de 4 séquence de travail permettant de progresser à la fois sur la compréhension globale du système, l'administration système sous-jacente et le dévelopement de cette nouvelle génération d'outils. 

### Introduction

L'objectif de cette séance est de lancer le hackathon et de présenter le concept des `interactives tools` de [Galaxy](https://galaxyproject.org/) pour que les participants bénéficient d'un background collectif.

Parmi les sujets qui seront abordés :
* Les avantages fonctionnels des `interactives tools`
* Le type d'applications élligibles à être déployées sous cette forme
* L'architecture informatique sous-jacente
* Le principe des interractions entre Galaxy et les `interactives tools`
* Les enjeux du point de vue de la ressource de calcul
* Les compétences et les étapes pour développer et déployer les `interactives tools`

### Configurer Galaxy I - Admin - TP du GTN

L'objectif de cette séance est de comprendre le fonctionnement des briques techniques permettant de mettre en place les interactive tools dans Galaxy.
Dans un premier temps nous déroulerons les tutoriaux du Galaxy Training Network permettant d'installer, par le biais d'Ansible, une instance Galaxy proposant des Interactive Tools tournant sur la même machine.
Dans un second temps, nous rentrerons dans les détails de chaque rôle Ansible pour comprendre les tâches et paramétrages sous-jacents effectués.  
Le TP se fera avec des VM mises en place par l'équipe d'animation. L'OS reste à définir.

Liens vers les tutos :
* Installation de Galaxy avec  Ansible : https://galaxyproject.github.io/training-material/topics/admin/tutorials/ansible-galaxy/tutorial.html
* Configuration de Galaxy et du serveur pour mettre en place les IT : https://galaxyproject.github.io/training-material/topics/admin/tutorials/interactive-tools/tutorial.html

**Prérequis** : une bonne connaissance du fonctionnnement de Galaxy, de son installation et des notions d'Ansible.
* Ansible tutorial : https://galaxyproject.github.io/training-material/topics/admin/tutorials/ansible/tutorial.html

### Configurer Galaxy II - Admin - lien avec cluster

Nous aurons un retour d'expérience sur le lancement des interactive tools sur un cluster (plateforme GenOuest et IFB). 

### Hackathon - dev

L'objectif de cette double scéance est de comprendre et prendre en main le processus de construction d'un `interactive tool` à partir d'exemples concrets, voire de son propre projet pour les participants les plus avancés. Initialement prévue en présentielle, nous avons souhaité maintenir cette session à distance !

**Cible** : 
les développeurs d'outils de visualisation de données (R, Python, ...) souhaitant enrichir des workflows Galaxy existants ou publier leur applications sur des plateformes de traitement publiques comme [usegalaxy.fr](usegalaxy.fr) ou [usegalaxy.eu](usegalaxy.eu).

**Prérequis** : 

* Avoir assisté au moins à la premières session (Introduction) pour avoir une première connaissance des piles logiciels utilisées.
* Avoir des connaissances de base en programmation (Python, R, Bash, ...), en containerisation Docker & en OS Linux.

**Contenu** :

*Matinée du mercredi 09 décembre (9:00 - 13:00)*
   > + Introduction - Franck & Yvan
   > + Reprise en main de la VM du hack - Lain & Anthony
   > + **UseCase 01** - Installation de l'`interactive tool` RStudio - All

*Matinée du jeudi 10 décembre (9:00 - 13:00)*
  > + **UseCase 02** - Developpement et intégration d'un `interactive tool` à choisir parmi plusieurs exemples issues des communautés Ecologie et Métabolomique  - All
  > + OU **UseCase 03** - **"Bring your own app"**<span style="color:red"></span>  - All


<span style="color:red"><sup>\*</sup></span> A propos de la session **"Bring your own app"** : Vous êtes développeurs sur des outils R, Rshiny ou Python, en lien avec la visualisation et connaissant Docker? Vous avez un developpement fonctionnel et ce développement a été containarisé (Dockerfile écrit et fonctionnel). Venez donc avec votre propre projet afin de l'intégrer en tant qu'`interactive tool` - Contactez nous !

## Ressources de calcul pour la formation

Les ressources de calcul nécéssaires à cette animation sont mises à disposition par [UseGalaxy.eu](https://usegalaxy.eu/)

## Localisation : visio

Vous trouverez ci-après le lien de connexion et le code d’accès pour rejoindre la séance du 07/12/2020 qui débutera à partir de 14h00 :
https://bbb.visio.inrae.fr/b/vir-ibp-shp-guo

Je vous invite à vous connecter 5 minutes avant le début de la visioconférence afin de tester votre connexion (son et image). Si vous avez des difficultés pour vous connecter à partir du lien, faites un copié-collé dans le navigateur de votre choix.

Pour le bon déroulement de la formation, merci de veiller à être dans un environnement calme et de disposer d’un casque avec micro.


## Inscriptions

20-25 participants

Remplir le formulaire suivant : https://framaforms.org/hackathon-gxit-2020-inscription-1602514404

## Recueil des consentements pour enregistrement et diffusion de présentations

Pour information, au début de certaines présentations, il vous sera demandé si vous êtes d'accord pour qu'elles soient enregistrées.   
En cas d'enregistrement, nous vous demanderons systématiquement si vous êtes d'accord pour diffuser des vidéos où vous apparaitriez (audio et vidéo).


## Contact

* geoc@groupes.renater.fr

<div align="center">

[<img src="https://www.inrae.fr/themes/custom/inrae_socle/logo.svg" height="140">](https://www.inrae.fr/)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
[<img src="https://ist.inrae.fr/wp-content/uploads/sites/21/2020/02/DipSO_logo-gradient-rvb-scaled.jpg" height="80">](https://ist.inrae.fr/list-a-inrae/dipso/)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
[<img src="https://www.mnhn.fr/sites/mnhn.fr/files/museum-national-d-histoire-naturelle_2.png" height="100">](https://www.mnhn.fr/)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
[<img src="https://www.france-bioinformatique.fr/wp-content/uploads/logo-ifb_small.png" height="100">](https://www.france-bioinformatique.fr/)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
[<img src="https://www.cnrs.fr/themes/custom/cnrs/logo.svg" height="100">](https://www.cnrs.fr/)


</div>
