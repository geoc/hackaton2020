# Galaxy Interactive Tools Hackathon du 7 au 10 décembre 2020

## Soyons connectés
- Visio, chat pour les questions ou demander les "rooms" privées : https://bbb.visio.inrae.fr/b/vir-ibp-shp-guo
- Solution de repli : https://global.gotomeeting.com/join/234962565 (Solution de rerepli meet.jit.si/vir-ibp-shp-guo)
- Plaquette: https://forgemia.inra.fr/geoc/hackaton2020 
- Sous salle 1 (pour les apartés) : https://rdv5.rendez-vous.renater.fr/gxit1
- Sous salle 2 : https://rdv4.rendez-vous.renater.fr/gxit2
  
 
## Encadrants 
- Yvan Le Bras - MNHN
- Elie Arnaud - MNHN
- Coline Royaux - CNRS-INEE
- Romain Dallet - CNRS - Membre IFB
- Lain Pavot - INRAE
- Anthony Bretaudeau - INRAE - Membre IFB
- Franck Giacomoni - INRAE
- Estelle Ancelet - INRAE
- Patrick Chabrier - INRAE
- Virginie Rossard - INRAE
- Eric Latrille - INRAE

## Menu :yum:


### Après-midi du lundi 7 décembre (14:00 - 17:00)

  + Introduction sur les IT - Patrick & Yvan


Slides
- lancement https://drive.google.com/file/d/1khwprBKxY9psz5zCP7ZJg8geUqMnAg6m/view
- intro https://drive.google.com/file/d/1lQ__SdJkJ0zqmFrc_imIl6VEvlloQqJy/view


Galaxy training material: https://training.galaxyproject.org/training-material


#### Questions
- C'est-à-dire qu'il faut forcément docker ou singularity installé sur le HPC pour utiliser 1 GIT ?

### Journée *admin* du mardi 08 décembre (9:00 - 17:30)
  + Notions sur Ansible - Estelle
  + Installation de Galaxy avec Ansible - Anthony
  + Ajout des GxIT - Anthony & Estelle

### Matinée *dev* du mercredi 09 décembre (9:00 - 13:00)
  + Introduction - Franck & Yvan
  + Reprise en main de la VM du hack - Lain & Anthony
  + UseCase 01 - Installation de l'interactive tool AskOmics - All


### Matinée *dev* du jeudi 10 décembre (9:00 - 13:00)
  + UseCase 02 - Developpement et intégration d'un interactive tool à choisir parmi plusieurs exemples issues des communautés Ecologie et Métabolomique  - All
  + UseCase 03 - "Bring your own app"  - All

# ----- Les VMs -----

## Connection

[Choisissez votre VMs](https://docs.google.com/spreadsheets/d/1yoTIstEmC6DXVEc-gX0YNyaR7QoDsb7TJZ3sk_K9oyc/edit#gid=0), mis en place par Helena Rasche sur des serveur allemands.
Puis `ssh ubuntu@gat-x.fr.training.galaxyproject.eu`
/!\ leur nombre est limité, si vous êtes simple auditeur ou ne participez qu'à l'introduction, merci de ne pas vous servir.
Après installation de galaxy, il reste 3,5 Go. Quelques VM disponibles avec + de places. 

## Naviguer dans le terminal
Byobu est installé sur les VMs qui permet avec quelques commandes de vous faciliter la vie.
`F2` : créer un nouvel onglet
`F3` et `F4` : naviguer dans les onglets
`exit` ou `Ctrl + D` : fermer un onglet
`F7` puis :arrow_up: et :arrow_down: : mode scrollback (pour voir l'intégralité d'un log ou fichier)
`Ctrl + C` : sortir du mode scrollback
Pour activer ou désactiver : 'byobu-disable' et 'byobu-enable'

## Docker : supprimer images et conteneurs
`sudo -iu galaxy` seul l'utilisateur galaxy peut exécuter docker
`docker ps` lister les conteneurs actifs (permet de repérer l'identifiant du conteneur)
`docker stop id_conteneur` arrêter un conteneur
`docker images` liste des images docker présentes sur la machine (permet de récupérer l'identifiant de l'image)
`docker image rm id_image` supprimer une image


# ----- Session *Introduction* -----

[Consulter la session *Introduction* enregistrée ICI !](https://bbb.visio.inrae.fr/playback/presentation/2.0/playback.html?meetingId=216c27a3ffef2fc5c88f2b37c7ed5ef7cd781efe-1607340592703)

## lundi 7 décembre (14:00 - 17:00)

__22 participants__


### 0. Droit à l'enregistrement
Voulez-vous que l'on vous enregistre ? Usage : pour ré-écouter la sessions en cas de problèmes techniques ou absences urgentes
Aucune réponse négative

### 1. Tour de table / ce qu'on fait, attentes
Voir ici  [Qui sommes nous ? ](#qui) 

Et :

Ancelet Estelle
- INRAE Toulouse / gestion projet / administration / dev => notamment admin-sys d'une instance Galaxy
- présente une partie session admin et en attentes du côté dev + avoir plus d'infos sur Ansible / I

Anne Laheurte
- CDD INRAE Narbonne / passer application ~tipol (micropoluant) vers Galaxy + dev de Galaxy tools
- Galaxy tout neuf, montage en compétence, certaines interrogations sur les interactive tools

Anthony Bretaudeau
- INRAE Rennes / Plateforme GenOuest / s'occupe de plusieurs instances Galaxy dont Genouest et FR
- Discussions avec gens plein d'idées chouettes

Christophe Antoniewski
- responsable plateforme bioinfo ArtBio -> utilisation Galaxy comme outil de base pour production analyse pour chercheurs. Intéresé par les outils et le framework lui-même. Interactive tools = avancée majeure par rapport à l'existant notament rapidité d'inclusion de serveur R ou Python, R Shiny = super intéressant.
- avoir discussions, comment participer à faire progresser ces IT.

Coline 
- équipe PNDB / développement outils Galaxy pour analyse de données en écologie. 
- Connais Galaxy mais pas du tout Interactive tools

Dominique Pelletier
- chercheure Ifremer / participe aux activités du PNDB
- voir si elle peut en connaitre plus sur Galaxy.

Elie 
- équipe PNDB / développement app Shiny MetaShARK
- portage app Shiny MetaShARK vers IT + aide au dev app Shiny

Eric Latrille
- Narbonne biotech environnement avec Jean-Claude et Virginie. Application en chimiométrie / spectres infrarouge / plateforme Galaxy chemflow adossé à un MOOC
- intérêts liens interactif / MOOC

Eric Casellas
- INRAE Toulouse / développeur et déploiement application autour de modèles de simulation dynamique / déjà créé des outils Galaxy / déjà créé des app Shiny
- voir contraintes et possibilités de Galaxy IT

Fabien Mareuil
- Institut Pasteur Ingénieur de Recherche/développeur web et logiciel au Hub de bioinformatique et biostatistique avec Rémi P. Instance Galaxy Pasteur depuis 2013/ environ 2800 utilisateurs. IT pas du tout utilisés à Pasteur cependant de plus en plus d'apps Shiny et Jupyter notebook sont développés à l'IP et nous sommes intéressé par les IT pour pouvoir les intégrer à l'instance Galaxy de l'IP.
- voir pour activer les IT à Galaxy 

JC Boulet
- ingé plateforme polyphénol Spectro de masse et RMN / instance Galaxy chemflow + qques outils spectro de masses sur instance perso
- IT et édition de sorties de Galaxy (car souvent sorites complexes sous forme de répertoires donc peu lisibles par utilisateur)

Jérémy Tournayre
- ingé INRAE Clermont Ferrand pour analyse donnée omics -> mise en place de pipelines et analyse statistiques
- IT pour les chercheurs.

Lain Pavot
- ingé développeuse logicielle INRAE notamment Python pour plateforme metabolisme / GxIT - Ansible - Docker notamment via outil R Shiny 
- voir les différents outils existants 

Nadia Goué
- plateforme Auvergne Bioinfo / utilisation infra mesocentre UCA via notamment instance Galaxy où admin-sys / lien avec bdd
- voir ce qu'il est possible de faire via GxIT et les proposer aux utilisateurs à l'UCA -> rendre autonome les utilisateurs via GxIT qui peuvent répondre à leurs besoins

Pat -> Patrice Dehais (merci ;) )
- Patrice Dever / ingé INRAE avec Sarah Maman gère une instance Galaxy Toulouse depuis 2012
- auditeur libre

Patrick Chabrier
- Animateur équipe INRAE Toulouse / Simulateurs (chèvres/troupeau/...) -> utilisation et exploration de ces simulateurs => utilisation Galaxy = vertueux
- compléter expérience de développeur (R, Python, containers) et à chaque fois interfaces étaient assez rigides/brutes, apport des IT pour ergonomie // co-organisateur et élève

Rémi Planel
- Ingénieur de Recherche au Hub de bioinformatique et biostatistique à l'Institut Pasteur / développeur web / instance Galaxy Pasteur
- intéressé par solutions Ansible

Sarah -> Sarah Maman
- IE Toulouse SIGENAE 
- auditrice libre

Virginie Rossard
- labo biotech environnement  plateforme Galaxy chemflow / MOOC / développe outils / administre serveur
- questions sur NGINX / sockets - mise en place IT / certificats

Virginie Lollier
- IE Nantes Bioinfo Plateforme BIPS / spectro de masse - RMN - spectroscopie . Mise en place plateforme Galaxy en interne à la plateforme
- découverte

Yann Guitton
- IR plateforme spectro de masse Nantes / workflow4metabolomics / chimiste de plateforme + s'essaye à la bioinfo / 
- interface avec communautés habitués en point and clic => gros besoins des users ! Travail avec Lain sur un premier outil IT d'app Shiny

Yvan Le Bras (MNHN-PNDB)
- IR MNHN / infrastructure Pôle national de données de biodiversité (PNDB) - UMS PatriNat / Station marine de Concarneau = première station marine au monde encore en activité ;)
- apprendre !!!!


Excusés (ou pas ;) )
- Amin Cheikhi
- Franck Giacomoni - j'suis en bas du markdown !!!
- Nils Paulhe
- Marie Lefebvre 


### 2. [Lancement](https://drive.google.com/file/d/1khwprBKxY9psz5zCP7ZJg8geUqMnAg6m/view?usp=sharing)

Organisateurs : contents d'avoir réuni pour ce hackathon une communauté originaire de plusieurs structures.
Présentation du réseau GEOC.

S'intéresser aux bonne spratiques : planemo (https://github.com/galaxyproject/planemo), intégration continue (GitLab CI/CD)

### 3. [Introduction](https://drive.google.com/file/d/1lQ__SdJkJ0zqmFrc_imIl6VEvlloQqJy/view?usp=sharing)

Son gros + : Galaxy a rendu accessible les Workflows (snake).
Pourquoi continue-t-il à plaire ? se base sur des standards : conda, conteneriser pour garantir l'accessibilité, etc.
Intégration d'outils de plus en plus utilisés : jupyter notebook, la communauté R avec RStudio et RShiny app qui offre une excellente intéractivité. 
Plusieurs voies pour galaxy pour offrir de l'intéractivité : 
 - Développement de Plugins basés sur D3
 - des outils spécifiques : trackster
 - Galxy Interactive Environments, GIE : basé sur une image docker et avec des input dataset
 - Galaxy Interactive Tools, GxIT : un outil classique
 - Les jobs peuvent exécutés sur des clusters type hpc

Trainings materials galaxy : https://training.galaxyproject.org/training-material

Intégrer dans un workflow un GxIT...

Persistence de sessions avec un IT : 
 - Jupyter : mettre dans l'historique et le relancer.
 - Shiny : possibilité d'avoir des configurations dans l'url

Les dockers ne sont pas forcément appréciés des clusters de calculs.

Tools classiques : singularity et docker
usegalaxy.fr : on souhaite lancer du singularity mais l'appli doit pouvoir être lancer en NON root. 

Exemples d'interactive tool pour une app Shiny :  
 - https://github.com/yvanlebras/tools-ecology/blob/master/tools/interactive/interactivetool_wallace.xml
 - https://github.com/yvanlebras/wallace-docker

Virginie Lollier : IT input et output. Nous : collection ; systèmes historiques ; comment récupère t-il les fichiers ? Il y a des varaibles globales de galaxy pour récupérer ces input et output. Les jeux de données s'affichent au fur et à mesures dans ton historique. 


# ----- Session *Admin* -----

[Consulter la session cours *Admin* enregistrée ICI !](https://bbb.visio.inrae.fr/playback/presentation/2.0/playback.html?meetingId=216c27a3ffef2fc5c88f2b37c7ed5ef7cd781efe-1607340592703)

### 0. Recommandations
Mettre votre Statut "éloigné" si vous êtes occupés :)

Les salles pour les apartés en cas de soucis divers (ex : problème sur la vm) :
 - Sous salle 1 : https://rdv5.rendez-vous.renater.fr/gxit1
 - Sous salle 2 : https://rdv4.rendez-vous.renater.fr/gxit2

Voulez vous revoir les bases d'Ansible ? Sondage qui va apparaitre en bas à droite de votre écran BBB, répondre oui ou non ! Réponse : 

![](https://i.imgur.com/S56KItJ.png)


### 1. Tour de table / ce qui peut nous empêcher de suivre cette journée
Estelle: éviter de trop se focaliser sur la présentation et manquer d'écoute
Anthony: nucléaire et connection
Christophe: réunion utilisteur de 12-13 + ifb 15-17
Pat:
Anne:Pas de pb 
Viginie : au Labo pas de pb
Coline : pb de bande passante potentiel
Eric : Narbonne il pleut
Lain : petite réunion vers 14h
Fabien M: à la maison
Jean-Claude: réunion vers 16h30
Patrick: scribe
Jérémy: 
Nadia: pas beaucoup de bande passante
Rémi: vers 11h logistique perso transfert travail
Marie: en // workshop
Sarah: à la maison + auditrice libre
Yann: arrivée 11h10...auditeur libre
Yvan: suivre un peu


## Introduction à Ansible

résultat du sondage 9 oui / 15

- [Diapos](https://training.galaxyproject.org/training-material/topics/admin/tutorials/ansible/slides.html)
- [Tutoriel](https://training.galaxyproject.org/training-material/topics/admin/tutorials/ansible/tutorial.html)

15 diapos quoi/pourquoi/comment

Le rôle est la brique de base.
Un enjeu savoir réutiliser des rôles
Théorie/fonctionnenment de base/ Vocabulaire

A noter que le dépôt d'Ansible s'appelle "galaxy" à ne pas confondre avec galaxyproject.

Ansible pour gérer l'état de la machine:
- état courrant
- évolution

Tout est documentable et versionné.

Répondre à la question de la traçabilité des évolutions d'une machine.

Faciliter la reconstruction d'une machine

gestion de configuration = reproductibilité de la complexité d'une config

Pas besoin de déployer de démon sur les machines adminitrés.

Ansible = opensource & soutenu par Red Hat.

Avantage = communauté très importante et aussi au sein de la communauté Galaxy

Passage au vocabulaire

- inventory file = liste structurée des machines servies(=administrées) + utilisateur de connection en ssh, le choix du nom des groupes est important, en particulier parce que le nom va être réutilisé à plusieurs endroit. A noter qu'une machine peut appartenir à plusieurs groupes. Notion de sous-groupe existante aussi

- module = fonction exécutable paramètrable...ligne de commande
- une tâche = invocation d'un module avec les paramètre...script
- module+tâche = couche d'abstration générique fournit un os générique en quelque sorte

- rôle = succession de tâche + fichiers + templates + variables + handlers

remarque : ce qu'un rôle peut faire un autre peut le défaire
- playbook = liste des rôles à appliquer sur un groupe de machines

Commencer à utiliser Ansible = réutiliser des rôles pour développer des playbook

Fonctionnalités supplémentaires:
- Vault pour stoquer des informations critiques https://github.com/ARTbio/GalaxyKickStart/blob/local_Mississippi/group_vars/Mississippi

1 ou plusieurs playbook?
Tout dépend?

Playbook general + playbook spécifique plus critique.

Les rôles sont déposés sur des repositories. Apprécier leur qualité..un peu comme d'habitude, popularité, expérience, tests.

Notion d'"idem potence"

L'unique prérequis = ssh

1 playbook peut échouer

Il existe un ansible lint qui contrôle la consistance du code.

Répertoires d'un role :
Default : variable par défaut ; ces variables vont pouvoir être modifiées.
Files : fichiers copier tel quel sur l'hote. Bout de code copié sur l'hote pour etre executé la bas. Fichiers de config que l'on veut déposer tel quel sur l'hote. Liste d'outils en yaml avec galaxyproject par exemple.
exemple de handler = redémarrer Apache
meta = information sur le rôle
readme= classique
tâche = pour comprendre lire le main
templates = contient les templates

Les machines listées dans ce groupe auront la même installation. Une machine peuvent être dans plusieurs groupe 
On peut aussi avoir des sous groupes

 - Module comme une ligne de commande
 - Tâche commme un script. 

=> Gros avantage d'Ansible : couche d'abstraction sur les commandes que tu peux avoir sur un système.

Tuto galaxyproject pour faire une session admin : https://training.galaxyproject.org/training-material/topics/instructors/tutorials/galaxy-admin-training/tutorial.html#everyone-bootstrapping-the-vms


Passage au tuto
15 minutes pour aller jusqu'à Facts

Atom & vscode fournissent du support dans l'édition de code "Ansible"

- https://atom.io/
- https://code.visualstudio.com/

Les facts = vocabulaire propre à Ansible



## Installation de Galaxy

- [Diapos](https://training.galaxyproject.org/training-material/topics/admin/tutorials/ansible-galaxy/slides.html)
- [Tutoriel](https://training.galaxyproject.org/training-material/topics/admin/tutorials/ansible-galaxy/tutorial.html)

Objectif = Un galaxy de base sur chacune des machines
Galaxy a besoin d'une base de donnée, d'espace disque, 
d'uwsgi qui fait le lien entre appli et python.

Généralement uwsgi n'est pas exposé directement en général.

CVMFS fait désormais parti de l'ecosysteme pour partager des donnees de référence entre les instances.

et finalement pulsar pour envoyer des jobs 

Pour ceux qui voudraient en savoir, inscriptions sessions virtuels jusqu'au 18 décembre Formation galaxy adminn : https://galaxyproject.org/events/2021-01-admin-training/

A terme offre de calcul potentiel des serveurs pulsar disponible pour les communautés

Classiquement on a point de montage commun entre galaxy et tous les noeuds de calcul.

L'utilisation https://galaxyproject.org/admin/objectstore/ est possible pour définir des espaces de données complexes et à plusieurs endroits.

Choisir où stoquer la BD. En local pour la vitesse et surtout prévoir de la place.

Quelques bonnes pratiques à respecter qui sont implémnetées dans le tuto.

l'approche Pulsar peut se faire en "//" de l'approche classique.

Le tuto, c'est parti, premier RDV à la fin de l'install de PostGres

mutable setup = fichier de conf gérer aussi par Galaxy

## Activation des GxIT

- [Diapos](https://training.galaxyproject.org/training-material/topics/admin/tutorials/interactive-tools/slides.html)
- [Tutoriel](https://training.galaxyproject.org/training-material/topics/admin/tutorials/interactive-tools/tutorial.html)


Tool config : comment on accède à l'application : port et url s'il faut ajouter un sufixe. 

Galaxy Interactive tools : GxIT
URL unique pour chaque contener : à la fin notre nom de domaine de notre galaxy.
DNS générique : blabla.notreSiteGalaxy doit référencé notreSiteGalaxy


Le proxy se sert de l'identifiant pour faire le lien avec le port dédié au container.



## Ajouter un IT local via Ansible

- [Diapos](https://docs.google.com/presentation/d/1kS5bOuwWcngKkqk1IRkES-VkDWqECLERlurOggLZpVQ/edit?usp=sharing)

Ajout de l'IT Ethercalc : à la fin du TP, une bonne dizaines de VM étaient fonctionnelles :blond-haired-woman: :blond-haired-man: :female-artist: :man-bowing: :woman-bowing: :man_dancing: :woman_in_lotus_position: :man_in_lotus_position: :blond-haired-woman: :blond-haired-man:

## Retour d'expérience : enregistrement DNS et certificat SSL génériques à l'INRAE

- [Diapos](https://docs.google.com/presentation/d/1-G04UutqIWoxgJCpGXXlv-UgepsFv0F6pDIEm-bTgdQ/edit?usp=sharing)

## Retour d'expérience : utilisation d'un cluster de calcul

- [Diapos](https://docs.google.com/presentation/d/1lu2xSaV5ibQCnlURzyk5ZiIA-EORZSipw74_Fz3pyLM/edit?usp=sharing)

par défaut les IT sont lançés en local.

Slurm très utilisé

Le serveur slurm doit accepter docker.
Seul Galaxy sur genouest a les droits docker.


Autre stratégie = noeuds dédiés.

Subtilité autour des cgroups.

Docker échappe aux contraintes positionnées sur un cgroup.

Judicieux d'utiliser un epilog script.

Utiliser les docker_run_extra_arguments.

Certaines images sont grosses 21 GB pour climat et jupyter.

Comment compartimenter l'accès au système de fichier = pulsar.

spécificité  de paramètrage si on utilise slurm et embeded pulsar.

Singularity? ça pourrait être possible.
Mais aussi en perspective Pulsar...



# ----- Session *Dev jour 1* -----

L'enregistrement de la session *Dev jour 1* est fusionné avec celui de la session *Admin*.
[Consulter la session cours *Dev jour 1* enregistrée ICI !](https://bbb.visio.inrae.fr/playback/presentation/2.0/playback.html?meetingId=216c27a3ffef2fc5c88f2b37c7ed5ef7cd781efe-1607340592703)

## Welcome - All GEOC

  - Tour de table / ce qui peut nous empêcher de suivre cette journée
      - Estelle Ancelet : Détendue ;-) et impatiente
      - Coline Royaux : pb de connexion attendue
      - Franck Giacomoni : Organisateur, INRAE
      - Lain Pavot : Organisateur, INRAE - s'amuser !!
      - Romain Dallet : Organisateur, ABIMS et il fait beau vite fait !
      - Virginie Rossard : Organisateur, impatiente de dev
      - Christophe Antoniewski : Participant - réunion vers 11h
      - Anne Laheurte : rien de particulier
      - Anthony Bretaudeau : Organisateur, frais et détendu
      - Doris Brockmann : INRAE Transfert, dev d'interactive Tools
      - Eric Casellas : INRAE, pas de souci - hate !
      - Eric Latrille : Organisateur - INRAE, Il fait beau à Narbonne
      - **LA PAUSE - la PAUSE - la PAUSE !!**
      - Fabien Mareuil : Pasteur - les enfants à la maison
      - JC Boulet : Organisateur - INRAE, 4G à l'INRAE
      - Elie Arnaud : Organisateur - MMHN, 
      - Jérémy Tournayre : INRAE, coupure d'elec :-/ 
      - Mélanie Pétéra : INRAE, pas présente lundi/mardi - pb de machine Win
      - Marie Lefebvre : INRAE au Pays-Bas
      - Nadia Goué : UCA - tous les TPs mardi, grand soleil à Clermont, pas trop de neige
      - Patrice Dehais - INRAE, SIGENAE - A la voie d'Estelle ;-)
      - Patrick Chabrier : Organisateur - INRAE tres interessé par la dev et entierement dispo
      - Rémi Planel : Pasteur, dispo - jupyter notebook
      - Sarah Maman : INRAE, SIGENAE
      - Yvan Le Bras : Organisateur - MMHN - Amoureux des GxIT et pecheur + compteur d'histoire d'encornets

## Introduction - Yvan

24 personnes ont assisté à cette session.

[Slides](https://drive.google.com/file/d/1lQ__SdJkJ0zqmFrc_imIl6VEvlloQqJy/view)

Yvan refait la présentation de lundi et une discussion se lance sur la reproductibilité lorsque l'on utilise des gxIT. Une bonne pratique est de prévoir dans l'interactive tool un enregistrement (traçage) des logs et des opérations qui sont faites sur les jeux de données. Cela se fait bien pour les applications Rshiny.

Il y a beaucoup d'utilisations imaginables des GxIT.


## Le point "VM" - Anthony et Lain

[Inventaires des VMs fraîches et dispo](https://docs.google.com/spreadsheets/d/1yoTIstEmC6DXVEc-gX0YNyaR7QoDsb7TJZ3sk_K9oyc/edit#gid=0)

## Mon premier GxIT : Installation de AskOmics ❅

### Installation du GxIT
Connexion à la machine:
 - `ssh ubuntu@gat-${NUMBER}.fr.training.galaxyproject.eu`

On récupère l'xml de l'outils qu'on place dans le répertoire "tools" de galaxy:
 - `mkdir -p /home/ubuntu/galaxy/files/galaxy/tools`
 - `cd /home/ubuntu/galaxy/files/galaxy/tools`
 - `wget https://raw.githubusercontent.com/galaxyproject/galaxy/dev/tools/interactive/interactivetool_askomics.xml`

### Configuration de galaxy

A PRIORI LA PREMIERE PARTIE barée N'EST PAS NECESSAIRE CAR LES OUTILS SONT AJOUTES DANS LE `tool_conf_interactive.xml.j2` DANS UNE SECTION D'OUTILS DEDIEE NOMMEE `Interactive Tools`

On ajoute le nom du xml de l'outils au fichier de conf d'ansible dans la section "galaxy_local_tools":
 - `cd /home/ubuntu/galaxy`
 - `vi group_vars/galaxyservers.yml`

Resultat final:
```
galaxy_local_tools:
  - interactivetool_askomics.xml
```

On ajoute le nom du xml de l'outils au fichier de conf d'ansible dans la section tool_conf_interactive.xml.j2:
 - `cd /home/ubuntu/galaxy`
 - `vi templates/galaxy/config/tool_conf_interactive.xml.j2`

Créer une nouvelle section à la fin du fichier yml comme montré ci-dessous :
Resultat final:
```<toolbox monitor="true">
    <section id="interactivetools" name="Interactive Tools">
        <tool file="interactive/interactivetool_ethercalc.xml" />
        <tool file="interactive/interactivetool_askomics.xml" />
    </section>
</toolbox>
```

Les outils interactifs, après le lancement avec playbook, à activer se trouvent dans /srv/galaxy/server/tools/interactive/ et sont activés dans /srv/galaxy/local_tools/.

Éditer le fichier /home/ubuntu/galaxy/templates/galaxy/config/job_conf.xml.j2
```diff
--- templates/galaxy/config/job_conf.xml.j2
+++ templates/galaxy/config/job_conf.xml.j2
     <destinations default="local">
         <destination id="local" runner="local"/>
-        <destination id="interactive_local" runner="pulsar_embedded">
+        <destination id="interactive_local" runner="local_plugin">
             <param id="docker_enabled">true</param>
             <param id="docker_volumes">$defaults</param>
             <param id="docker_sudo">false</param>
             <param id="docker_net">bridge</param>
             <param id="docker_auto_rm">true</param>
             <param id="docker_set_user"></param>
             <param id="require_container">true</param>
-            <param id="container_monitor_result">callback</param>
-            <env id="REQUESTS_CA_BUNDLE">/etc/ssl/certs/ca-certificates.crt</env>
         </destination>
     </destinations>
     <tools>
        <tool destination="interactive_local" id="interactive_tool_ethercalc" />
+        <tool destination="interactive_local" id="interactive_tool_askomics" />
     </tools>
```

Ce qui obtenu à la fin, pour le fichier job_conf.xml.j2 (pour les personnes qui n'ont pas fait la partie admin - qui sont parties d'une machine neuve):
```xml=
<job_conf>
    <plugins workers="4">
        <plugin id="local_plugin" type="runner" load="galaxy.jobs.runners.local:LocalJobRunner"/>
<plugin id="pulsar_embedded" type="runner" load="galaxy.jobs.runners.pulsar:PulsarEmbeddedJobRunner">
    <param id="pulsar_config">/srv/galaxy/config/pulsar_app.yml</param>
</plugin>

    </plugins>
    <destinations default="local_destination">
        <destination id="local_destination" runner="local_plugin"/>
        <destination id="interactive_local" runner="local_plugin">
            <param id="docker_enabled">true</param>
            <param id="docker_volumes">$defaults</param>
            <param id="docker_sudo">false</param>
            <param id="docker_net">bridge</param>
            <param id="docker_auto_rm">true</param>
            <param id="docker_set_user"></param>
            <param id="require_container">true</param>
        </destination>
    </destinations>
    <tools>
        <tool destination="interactive_local" id="interactive_tool_ethercalc" />
        <tool destination="interactive_local" id="interactive_tool_askomics" />
    </tools>
</job_conf>
```
Il faut que l'id du runner LocalJobRunner (id="local_plugin" ici, soit utilisé dans la destination de interactive_local runner="local_plugin").
Pour récapituler,
`<plugin id="local_plugin" type="runner" load="galaxy.jobs.runners.local:LocalJobRunner"/>`
`<destination id="local_destination" runner="local_plugin"/>`
on voit que la destination référence le runner local en utilisant son ID.

Passer le user en admin: (Hot Fix) - - /!\ pas necessaire pour utiliser les GxIT
 - Créer un compte avec le mail "someone@example.org" sous votre instance galaxy (qu'importe le mdp).
 - OU ajouter votre mail utilisateur dans groupvars/galaxyservers.xml : `admin_users`. 

### On fait regénérer la configuration de galaxy par ansible
 - `cd /home/ubuntu/galaxy`
 - `ansible-playbook galaxy.yml`

Galaxy va être redémarré par ansible, et dès que le playbook est fini le resultat devrait être dispo sur https://gat-${NUMBER}.fr.training.galaxyproject.eu

L'outil Askomics se retrouve dans le menu de gauche mais en dehors du menu d'interactive tools.

### Pour visualiser vos interactives tools :
 - Depuis l'interface galaxy, 'Active interactive tools' 
 - `sudo docker ps`

### Démo avec Askomics
Fichier source `gene.tsv` : https://pfem.clermont.inra.fr/pydio/public/askomics (pwd askomics)
 
### Erreurs générées

 - Proxy target missing : se connecter en admin user, créer une clé api, lancer l'interactive tool avec le fichier de données gene.tsv.
 - 
Ca marche si on a pas l'image askomics. Si on relance un IT et qu'on a déjà l'image askomics, on a l'erreur proxy. Pour que ça fonctionne j'ai du supprimer l'image et le container et tout a, à nouveau, fonctionné
  - `sudo docker images` pour obtenir l'id de l'image
  - `sudo docker rmi IDimage`
On peut alors relancer l'IT via Galaxy.


 - FileNotFoundError -> lier au mode `pulsar_embedded` du destination dans le job_conf.xml (Cf plus haut)
```sh
   Error: No such container: 19ba73c56440499e80da6924a1cbcb32
Traceback (most recent call last):
  File "/askomics/cli/upload_files.py", line 130, in <module>
    UploadFiles().main()
  File "/askomics/cli/upload_files.py", line 47, in __init__
    starter.start()
  File "/askomics/askomics/libaskomics/Start.py", line 39, in start
    self.create_data_directory()
  File "/askomics/askomics/libaskomics/Start.py", line 46, in create_data_directory
    os.makedirs(self.data_directory)
  File "/usr/lib/python3.6/os.py", line 220, in makedirs
    mkdir(name, mode)
FileNotFoundError: [Errno 2] No such file or directory: ''
```

### Resources
 - [Documentation d'AskOmics](https://flaskomics.readthedocs.io/en/4.0.0/galaxy/) ;
 - [Dockerfile](https://hub.docker.com/r/askomics/flaskomics-with-dependencies/dockerfile) ;
 - [Dockerhub](https://hub.docker.com/r/askomics/flaskomics-with-dependencies) ;
 - [Original Github repo](https://github.com/askomics/askomics) ;
 - [Tool's XML](https://raw.githubusercontent.com/galaxyproject/galaxy/dev/tools/interactive/interactivetool_askomics.xml).


## Mon 2e GxIT : Installation d'un outil R shiny ❅❅

Dans cette partie, on va creer le Dockerfile permettant de définir à quoi va ressembler l'image docker de notre outils, ainsi que le XML de l'outils!

### Les bases

#### Docker
Explications de ce qu'apporte docker en terme d'isolation - freestyle orale en 5 points:
 - isolation du filesystem ;
 - isolation des users/groups ;
 - isolation des processus ;
 - isolation réseau ;
 - plus de détails ici: https://medium.com/@BeNitinAgarwal/understanding-the-docker-internals-7ccb052ce9fe

Explication orales Docker != VM (very important)

#### Les XML des outils galaxy
Ils sont rédigés comme pour n'importe que autre outils, à une différence près: `<tool ... tool_type="interactive">`
On a donc de façon typique:
 - des requirements ;
 - des entry points ;
 - une commande de lancement
 - des inputs ;
 - des outputs ;
 - des tests ;
 - un help ;
 - des citations. 


### Choix 01 - outil GeoExplorer by Yvan
Une app depuis https://github.com/yvanlebras/tools-ecology/tree/master/tools/interactive

- Par exemple [Geoexplorer](https://github.com/yvanlebras/tools-ecology/blob/master/tools/interactive/interactivetool_geoexplorer.xml) et docker lié https://github.com/yvanlebras/geoexplorer-docker/blob/master/Dockerfile

Example input file (csv)::
"ID"	"x"	"y"	"test"
01	-60.291838	46.328137	2
02	-114.58927	35.022485	3
03	-93.37406	30.00586	4
04	-79.336288	43.682218	5
05	-109.156024	31.904185	2
06	-71.098031	42.297408	9
07	-110.927215	32.18203	12

#### Installation du GxIT GeoExplorer (App Shiny visualisation cartographique et variables)

 On récupère l'xml de l'outils qu'on place dans le répertoire "tools" de galaxy:
 - `cd /home/ubuntu/galaxy/files/galaxy/tools`
 - ` wget https://raw.githubusercontent.com/yvanlebras/tools-ecology/master/tools/interactive/interactivetool_geoexplorer.xml`
 
#### Configuration de galaxy 
 
 On ajoute le nom du xml de l'outils au fichier de conf d'ansible dans la section tool_conf_interactive.xml.j2:
 - `cd /home/ubuntu/galaxy`
 - `vi templates/galaxy/config/tool_conf_interactive.xml.j2`

Créer une nouvelle section à la fin du fichier yml comme montré ci-dessous :
Resultat final:
```<toolbox monitor="true">
    <section id="interactivetools" name="Interactive Tools">
        <tool file="interactive/interactivetool_ethercalc.xml" />
        <tool file="interactive/interactivetool_askomics.xml" />
        <tool file="interactive/interactivetool_geoexplorer.xml" />
    </section>
</toolbox>
```

Éditer le fichier /home/ubuntu/galaxy/templates/galaxy/config/job_conf.xml.j2
```diff
--- templates/galaxy/config/job_conf.xml.j2
+++ templates/galaxy/config/job_conf.xml.j2
     <destinations default="local">
         <destination id="local" runner="local"/>
-        <destination id="interactive_local" runner="pulsar_embedded">
+        <destination id="interactive_local" runner="local_plugin">
             <param id="docker_enabled">true</param>
             <param id="docker_volumes">$defaults</param>
             <param id="docker_sudo">false</param>
             <param id="docker_net">bridge</param>
             <param id="docker_auto_rm">true</param>
             <param id="docker_set_user"></param>
             <param id="require_container">true</param>
-            <param id="container_monitor_result">callback</param>
-            <env id="REQUESTS_CA_BUNDLE">/etc/ssl/certs/ca-certificates.crt</env>
         </destination>
     </destinations>
     <tools>
        <tool destination="interactive_local" id="interactive_tool_ethercalc" />
        <tool destination="interactive_local" id="interactive_tool_askomics" />
+       <tool destination="interactive_local" id="interactive_tool_geoexplorer" />
     </tools>
```

Ce qui obtenu à la fin, pour le fichier job_conf.xml.j2 (pour les personnes qui n'ont pas fait la partie admin - qui sont parties d'une machine neuve):
```xml=
<job_conf>
    <plugins workers="4">
        <plugin id="local_plugin" type="runner" load="galaxy.jobs.runners.local:LocalJobRunner"/>
<plugin id="pulsar_embedded" type="runner" load="galaxy.jobs.runners.pulsar:PulsarEmbeddedJobRunner">
    <param id="pulsar_config">/srv/galaxy/config/pulsar_app.yml</param>
</plugin>

    </plugins>
    <destinations default="local_destination">
        <destination id="local_destination" runner="local_plugin"/>
        <destination id="interactive_local" runner="local_plugin">
            <param id="docker_enabled">true</param>
            <param id="docker_volumes">$defaults</param>
            <param id="docker_sudo">false</param>
            <param id="docker_net">bridge</param>
            <param id="docker_auto_rm">true</param>
            <param id="docker_set_user"></param>
            <param id="require_container">true</param>
        </destination>
    </destinations>
    <tools>
        <tool destination="interactive_local" id="interactive_tool_ethercalc" />
        <tool destination="interactive_local" id="interactive_tool_askomics" />
        <tool destination="interactive_local" id="interactive_tool_geoexplorer" />
    </tools>
</job_conf>
```
Il faut que l'id du runner LocalJobRunner (id="local_plugin" ici, soit utilisé dans la destination de interactive_local runner="local_plugin").
Pour récapituler,
`<plugin id="local_plugin" type="runner" load="galaxy.jobs.runners.local:LocalJobRunner"/>`
`<destination id="local_destination" runner="local_plugin"/>`
on voit que la destination référence le runner local en utilisant son ID.

#### On fait regénérer la configuration de galaxy par ansible
 - `cd /home/ubuntu/galaxy`
 - `ansible-playbook galaxy.yml`

Galaxy va être redémarré par ansible, et dès que le playbook est fini le resultat devrait être dispo sur https://gat-${NUMBER}.fr.training.galaxyproject.eu

L'outil Geoexplorer se retrouve dans le menu de gauche mais en dehors du menu d'interactive tools.

#### Démo avec Geoexplorer
Fichier source `Geoexplorer_input_ok` : https://data-access.cesgo.org/index.php/s/tpWO2Lr69ONyy5w


Pour voir une app Shiny utilisant l'API Galaxy pour importer/exporter des données Galaxy : https://github.com/yvanlebras/wallace-docker

# ----- Session Dev -  jour 2 -----

[Consulter la session cours *Dev jour 2* enregistrée ICI !](https://bbb.visio.inrae.fr/playback/presentation/2.0/playback.html?meetingId=216c27a3ffef2fc5c88f2b37c7ed5ef7cd781efe-1607340592703)

20 participants à cette session :+1: 


## Intégration de Heatmap, un application R Shiny

### Recette de HeatMap ?
 - un [dockerfile](https://raw.githubusercontent.com/workflow4metabolomics/gie-shiny-heatmap/master/Dockerfile)
  - un xml
  - une pincée de conf
  - Le [script R](https://raw.githubusercontent.com/workflow4metabolomics/gie-shiny-heatmap/master/app.R) contenu dans l'image et qui fait tourner shiny

### Découverte guidée du dockerfile - Lain

source : [dockerfile](https://raw.githubusercontent.com/workflow4metabolomics/gie-shiny-heatmap/master/Dockerfile)

Github: [heatmap](https://github.com/workflow4metabolomics/gie-shiny-heatmap/)

Mais d'abord, qu'est-ce qu'un dockerfile? Et une image docker? Et un container? Bref, c'est quoi docker?

Commentaire de Lain sur la structure du dockerfile :

```txt
Docker, A quoi cela sert?
Isoler un processus en construisant un systeme de fichier avec isolation des processus basé sur des namspaces.
Un docker n'est pas une VM.
Un container sur une machine linux ne pourra que faire tourner des programmes natifs.
Un docker file sert à construire un systeme de fichier avec les ressources necessaires pour executer les actions définies




```

```txt

### image de base dans laquelle on va 
FROM quay.io/workflow4metabolomics/gie-shiny:latest

# Installing packages needed for check traffic on the container and kill if none
RUN apt-get update && \
    apt-get install --no-install-recommends -y r-cran-car

# Install R packages
COPY ./packages.R /tmp/packages.R
RUN Rscript /tmp/packages.R

# Build the app
RUN rm -rf /srv/shiny-server/sample-apps && \
    rm /srv/shiny-server/index.html && \
    mkdir -p /srv/shiny-server/samples/heatmap && \
    chmod -R 755 /srv/shiny-server/samples/heatmap && \
    chown shiny.shiny /srv/shiny-server/samples/heatmap

COPY ./app.R /srv/shiny-server/samples/heatmap/app.R
COPY ./static/css/styles.css /srv/shiny-server/samples/heatmap/styles.css

```


**Notes :** 

Recommandations pour les numerotations des versions

Il y  a 2 dépots docker : 
https://github.com/abretaud/geoc_gxit_ansible/
hub.docker.com et http://quay.io/

Le port par défaut de RShiny dockerisé est le 3838

Les environment_variables seraient nécessaires pour récupérer des données provenant de l'historique Galaxy. Dans l'exemple, ce n'est donc pas nécessaire.

Pour vérifier la syntaxe xml
https://www.xmlvalidation.com/ ou https://www.w3schools.com/xml/xml_validator.asp


### "C'est l'heure de mettre la console dans le cambuis" (XML + conf)

A vous de jouer et de contruire le bon xml pour l'outil HeatMap

Vous pourrez prendre exemple sur l'[xml d'askomics](https://github.com/galaxyproject/galaxy/blob/dev/tools/interactive/interactivetool_askomics.xml)

  - groupe 01 : Estelle / Christophe / Eric L. / Jérémy / Patrice / Anthony
      - lien room privée : https://rdv5.rendez-vous.renater.fr/rs0w1-rxxod-iooxh
      - lien hackmd privé : https://hackmd.io/@fgiacomoni/Groupe01HackGxIT/edit
  - groupe 02 : Coline / Anne / Fabien / Mélanie / Patrick / 
      - lien room privée : https://rdv3.rendez-vous.renater.fr/bogzp-1l0bg-qklf9
      - lien hackmd privé : https://hackmd.io/@fgiacomoni/Groupe02HackGxIT/edit
  - groupe 03 : Romain / Doris / Jean-Claude / Marie / Rémi / 
      - lien room privée : https://rdv4.rendez-vous.renater.fr/n3rhr-a5u5m-hbuct
      - lien hackmd privé : https://hackmd.io/@fgiacomoni/Groupe03HackGxIT/edit
  - groupe 04 : Virginie / Eric C. / Elie / Nadia / Sarah /
      - lien room privée : https://rdv5.rendez-vous.renater.fr/jkxxr-8wwdl-mxuxk
      - lien hackmd privé : https://hackmd.io/@fgiacomoni/Groupe04HackGxIT/edit

Pour la conf, utiliser ce que vous avez vu mercredi ;-) 
Top chrono - le premier qui affiche l'IT en question

![chronometre](https://i.imgur.com/pXWdClv.png)

### Solution de configuration de galaxy

On ajoute l'xml dans /home/ubuntu/galaxy/files/galaxy/tools/ .

On ajoute le nom du xml de heatmap au fichier de conf d'ansible dans la section "galaxy_local_tools":
 - `cd /home/ubuntu/galaxy`
 - `vi group_vars/galaxyservers.yml`

Resultat final:
```
galaxy_local_tools:
  - interactivetool_askomics.xml
  - interactivetool_geoexplorer.xml
  - interactivetool_heatmap.xml
```

Ensuite, on ajoute une entrée au tool_conf_interactive:
 - `cd /home/ubuntu/galaxy`
 - `vi templates/galaxy/config/tool_conf_interactive.xml.j2`

et on ajoute une entrée dans la section des interactivetools:
```xml
    <section id="interactivetools" name="Interactive Tools">
        <tool file="interactive/interactivetool_ethercalc.xml" />
        <tool file="interactive/interactivetool_askomics.xml" />
        <tool file="interactive/interactivetool_geoexplorer.xml" />
        <tool file="interactive/interactivetool_heatmap.xml" />
    </section>
```

Éditer le fichier /home/ubuntu/galaxy/templates/galaxy/config/job_conf.xml.j2 pour y ajouter la destination d'exécution de notre interactive tool:
```xml
  <tools>
    <tool destination="interactive_local" id="interactive_tool_ethercalc" />
    <tool destination="interactive_local" id="interactive_tool_askomics" />
    <tool destination="interactive_local" id="interactive_tool_geoexplorer" />
    <tool destination="interactive_local" id="interactive_tool_heatmap" />
  </tools>
```

Ensuite, on lance ansible-playbook.yml

### Solution du XML

```xml=


<!--
alors ici, il faut mettre une balise
-->



<!--
ici aussi
-->



<!--
Et puis là, peut-être, si vous voulez,
mais c'est pas sure.
-->

<!--

Une des solution apportée fonctionne!
Et doc, la voici!
Il s'agit bien sur de l'une des solutions possible.
D'autres sont tout autant valable!  ;)

-->

<tool id="interactive_tool_heatmap" tool_type="interactive" name="heatmap" version="0.1">
    <description>An interactive tool for heatmap</description>
    <requirements>
        <container type="docker">emetabohub/heatmap</container>
    </requirements>
    <entry_points>
        <entry_point name="heatmap" requires_domain="True">
            <port>8765</port>
            <url>/</url>
        </entry_point>
    </entry_points>
    <environment_variables>
        <environment_variable name="HISTORY_ID">$__history_id__</environment_variable>
        <environment_variable name="REMOTE_HOST">$__galaxy_url__</environment_variable>
        <environment_variable name="GALAXY_WEB_PORT">8080</environment_variable>
        <environment_variable name="GALAXY_URL">$__galaxy_url__</environment_variable>
    </environment_variables>
    <command><![CDATA[

        mkdir -p /srv/shiny-server/samples/heatmap &&
        cp '$infile' /srv/shiny-server/samples/heatmap/inputdata.tsv &&
        cd /srv/shiny-server/samples/heatmap/ &&
        Rscript app.R > '$outfile'
 
    ]]>
    </command>
    <inputs>
        <param name="infile" type="data" format="tabular,csv" label="tsv file"/>
    </inputs>
    <outputs>
        <data name="outfile" format="txt"/>
    </outputs>
    <tests>
    </tests>
    <help>
<![CDATA[

De l'Aide c'est pratique.

]]>
    </help>

</tool>
```





## C'est la fin du hackathon - je suis rentrée chez moi, comment je fais pour créer/tester mes IT, maintenant :crying_cat_face: ?

On va installer un environnement de développement sur nos machines! :scream_cat:

### Installation de docker (ubuntu)
 - `curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`
 - `sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"`
 - `sudo apt-get update`
 - `sudo apt-get upgrade`
 - `sudo apt-get install docker-ce`
 - `sudo usermod -aG docker $USER ## on ajoute l'user courant au groupe docker. Ça permet de lancer les commandes docker sans utiliser le sudo. Et ça, c'est cool!`
Pour que l'ajout au groupe docker soit pris en compte tout de suite, vous pouvez taper la commande `newgrp docker`. Cependant, je vous conseil de vous log out, et de vous re-login. Ce sera plus simple.

Se référer à https://docs.docker.com/engine/install/ pour les autres distributions/OS.

### Installation de galaxy
 - `sudo apt-get install git`
 - `mkdir ~/GxIT && cd ~/GxIT`
 - `git clone https://github.com/galaxyproject/galaxy`
 - `cd galaxy/config`
 - `cat galaxy.yml.interactivetools > galaxy.yml`
 - `cat job_conf.xml.interactivetools > job_conf.xml`
 - `cat tool_conf.xml.sample > tool_conf.xml`

### Configuration de galaxy

Editer ~/GxIT/galaxy/config/galaxy.yml:
```yml
uwsgi:
	## dans la section http, remplacer localhost par 0.0.0.0
	http: 0.0.0.0:8080

galaxy:
	## ajouter
	galaxy_infrastructure_url: http://localhost:8080
```

Editer ~/GxIT/galaxy/config/tool_conf.xml, ajouter:
```xml
  <section id="interactivetools" name="Interactive tools">
  </section>
```

#### Run galaxy! Ruuun!
 - `cd ~/GxIT/galaxy`
 - `./run.sh ## La première fois, ça prend du temps`

#### Installer un IT
Etape 1: Mettez le XML dans ~/GxIT/galaxy/tools/interactive/
Etape 2: ajoutez dans la section interactivetools de ~/GxIT/galaxy/config/tool_conf.xml:
```xml
    <tool file="interactive/interactivetool_monInteractiveTool.xml" />
```
Ce qui donne, par exemple, avec AskOmics:
```xml
  <section id="interactivetools" name="Interactive tools">
    <tool file="interactive/interactivetool_askomics.xml" />
  </section>
```
Etape 3: Rechargez la page web de galaxy :smile_cat: 


## Mon 3e GxIT : Installation de mon propre outil R shiny ❅❅❅❅❅

### planemo et les IT

Ne pas en espèrer trop pour l'instant?

## test de planemo serve

* sur une installation locale de galaxy opérationnelle pour les IT (http://localhost:8080/api/version => {"version_major": "21.01", "extra": {}})
* avec la dernière version de planemo (planemo --version => "planemo, version 0.73.0.dev0")

```
cd ~/DEVS/python36Galaxy/galaxy/tools
planemo serve --galaxy_root ~/DEVS/python36Galaxy/galaxy
```

ça donne :
```
...
assert app.config.interactivetools_enable, ValueError('Trying to load an InteractiveTool, but InteractiveTools are not enabled.')
AssertionError: Trying to load an InteractiveTool, but InteractiveTools are not enabled.
...
```
*Intuition :*

planemo prend la main sur la configutaion de l'instance locale de galaxy et "désactive" le paramètre *interactivetools_enable*



### Discussion des use-cases des participants

## Discussion/Perspectives et cloture du hack / 12h30-13h

Fabien et Rémi parlent d'un jupyter-notebook où il faudrait tester Pulsar pour isoler l'IT.

Yvan propose d'avoir à disposition des GxIT Rstudio thématiques avec les bonnes librairies.
Christophe est d'accord avec ça.
Par contre, le reporting et la traçabilité de ce qui est fait dans l'application Rstudio est un peu faible. La tentation est de tout faire dans RStudio et dans ce cas il n'y a plus d'intérêt d'utiliser Galaxy.
Une solution serait d'avoir une output supplémentaire de log et de récupérer des rmd.
On peut aussi faire une diff entre les données avant qu'elles n'arrivent dans l'application et à la sortie.

Christophe a 2 projets de RShiny, un pour l'analyse single cell et un autre, à plus long terme, pour la génération de rapport d'analyse en regroupant des graphiques.

Au sein de Metabohub, Mélanie parle d'un projet de rapport de synthèse qui récupèreraient les workflows. Ce n'est pour l'instant qu'une liste au Père Noël.
Sur ce thème, il y a aussi un projet Galaxy.
Actuellement, on peut aussi utiliser les pages Galaxy.

### Tour de table des participants sur le bilan

Chemflow : besoin d'outils de visualisation intéractifs de graphiques de données spectrales.

*Virginie* : super ; très intéressée par les interactives tools pour fouiller les spectres. Besoin en chimiométrie. Les outils interactifs serviraient à visualiser et fouiller puis une fois que l'on sait ce que l'on veut on retourne dans galaxy pour valider et tracer. 

*Estelle* : un peu dur de voir le ressenti des personnes ; pas de vision sur interactive tool ; peut-être à coté

*Nadia* : pas de demande particluière pour la plateforme. Pas de dev dans les mois qui viennent. Est ce utile d'ajouter une couche ? Possibilité de fournir le service RStudio et RShiny. Ce qui m'inquiète : la gestion de log et garder la reproductibilité. 

*Elie* : j'ai bien apprécié l'ensemlble de la formation. Avoir un détail de l'ensemble de l'écriture est bien plus parlant que les tutos qiu étaient déjàà mis à disposition. J'ai juste eu un petit souci perso qui m'a pas permis d'en profiter à fond mais je me rattraperai avec ce qui a été enrgistré. Sinon, comme Yvan l'a déjà souligné, on a déjà une idée d'applications Shiny à encapsuler pour récolter les métadonnées d'un jeu de données de biodiv. Merci aux orgas !

*Jeremy* : merci aux orga d'être réactifs ; Mieux aimé ce matin d'être en petit groupe. Ansible lent. Avoir des bogues permet de voir comment de déboguer.

*Fabien* : Préféré cette matinée à mettre les main dans le cambouis. J'ai apprécié cette formation. Merci !

*Mélanie* : j'ai bien aimé le découpage entre hier et aujourd'hui. Bien hier, pour évaluer le potentiel en regardant l'existant. Aujourd'hui bien pour le côté TP. Potentiel du côté métabo pour avoir des outils interactifs. Mais attention à la reproductibilité. Formule : outil qui sert à visualiser et une fois que l'on sait ce que l'on veut on retourne dans galaxy pour valider et tracer. 

*Eric L* : très bien de s'interroger aujourd'hui sur chaque mot du xml de ce qui se passe derrière. Après ce hackaton ? Rencontres informels ? Mise ne place d'un des tool ? Faire des ateliers autour d'une mise en place pour une plateforme. Visualisation des spectres : Identifier par des clics souris sort dans unn fichier qui se retrouve dans galaxy qui nous permettra de sélectionner les spectres. Autre application autour des molécules organiques. Sélectionner des données à partir d'autres BD. Pas emballer par Ansible. Outil qui permet d'avoir une tracabilité pour avoir une bonne vision de la config du serveur. 
*Franck* : Integrative tools : juste pour la modification pour de la sous sélection, un emanière de ocntourner notre problème de reproductibilité. 

*Eric C* : pas d'objectifs de production actuel.

*Christophe* : courbe d'apprentissage assez forte pour Ansible. Quand tu maitrises ansible, tu comprends mieux ton infrastructure. Quand on développe il est bien d'éloigner Ansible. Je préfère les hackatons en distanciel car tout est écrit, tu peux aussi t'absenter et revenir et tout est écrit. J'ai découvert BBB et je suis fan. 

*Patrick C* : en phase par tout ce qui a été dit. Workflow dans les interactives tools ? Risque de l'empilement des couches ? Nous avosn fait du code et cela a fonctionné. Mélanger la communauté des admin et dev bien pour voir les bonnes pratiques. Planemo lint : ok mais pas serve. Nous n'avons pas d'outil critique à mettre dans les outils interactifs. Super content des cette animation. 

*Yvan* : bon les crepes ;) Merci aux orgas malgré ces temps dur au vu de la distance. Tout n'est pas carré car les outils interactifs ne sont pas encore mûrs. 

*Coline* : Je vais devoir y aller donc je vous écrit ici pour ce tour de table : J'ai trouvé le hackathon super intéressant (malgré mes nombreux soucis de réseau haha), merci aux intervenants ! J'ai appris pleins de choses donc ça a été un peu fatiguant mais de la bonne fatigue du coup ! Je trouve le format demi-journée vraiment adapté quand c'est à distance comme ça, ça évite d'avoir trop la tête comme un compteur à la fin de la journée ;) Pour les IT pas sûre d'avoir encore les compétences en dev pour Shiny, Jupiter etc ... pour en développer from scratch mais au moins je sais comment la structure est organisée et comment en installer ce qui est déjà énorme je trouve ! Merci !


Nous lancerons un sondage  : Peut-on se donner rdv dans 3 mois pour faire un retour d'expérience ?


# -----  <a id="qui">Qui sommes nous ?</a> -----

__Par ordre alphabétique :__

## Amin 	Cheikhi	
Localisation : University of Pittsburgh	
Cours suivis : Introduction sur les IT dans Galaxy	

## Ancelet Estelle 
Localisation : INRAE Toulouse
Fonction : animatrice
Cours suivis : Introduction sur les IT dans Galaxy, Mettre en place un serveur Galaxy avec IT (aka admin), Développer des IT pour Galaxy (aka dev)
Profil : administration SI
Langages : Python
Expérience Galaxy : administration d'un serveur d'une dizaine d'utilisateurs, wrapping d'outils

## Antoniewski	Christophe	
Localisation : IBPS / CNRS	Paris
Cours suivis : Développer des IT pour Galaxy (aka dev)	
Profil : 
Lanagage : python, R	
Expérience Galaxy : arpbio

## Arnaud	Elie	
Localisation : MNHN	
Fonction : animateur
Cours suivis : Introduction sur les IT dans Galaxy, Développer des IT pour Galaxy (aka dev)	
Profil :
Langages : R	
expérience : métashark : dev en shiny ; couteneriser des applis
Attente : portage de cette appli sur galaxy ; 

## Boulet	Jean-Claude	
Localisation : INRAE Montpellier / Narbonne
Fonction : animateur
Cours suivis : Introduction sur les IT dans Galaxy, Développer des IT pour Galaxy (aka dev)
Profil :
Langages : 
Expérience Galaxy : plateforme polyphénol avec des données RMN et une de chimiométrie autour de l'instrance chemflow sortie en 2016 ; plateforme autour des spectres de masses. 
Attente : explorer dynamiquement des sorties complexes, type structures Matlab
YLB : Quand Beaucoup d'échantillons : multiQC

## Bretaudeau Anthony
Localisation : 	INRAE Rennes	
Fonction : animateur de la partie administration
Cours suivis : Introduction sur les IT dans Galaxy, Mettre en place un serveur Galaxy avec IT (aka admin), Développer des IT pour Galaxy (aka dev)
Profil : 
Langages : Python, js, espagnol	
Expérience Galaxy : admin et dev de la plateforme GenOuest

## Brockmann	Doris	
Localisation : INRAE Transfert Narbonne 	
Cours suivis : Développer des IT pour Galaxy (aka dev)	
Profil : 
Langages : R	
Expérience Galaxy : installation des gxIT en localhost

## Casellas	Eric	
Localisation : INRAE Toulouse
Cours suivis : Introduction sur les IT dans Galaxy, Développer des IT pour Galaxy (aka dev)
Profil : dev
Langages : C++, R, Fortran	
Expérience Galaxy : a few "standard" components using custom docker images

## Chabrier	patrick	
Localisation : INRAE Toulouse
Fonction : animateur & apprenant
Cours suivis : Introduction sur les IT dans Galaxy, Mettre en place un serveur Galaxy avec IT (aka admin), Développer des IT pour Galaxy (aka dev)
Profil : developpement d'application autour des modèles dynamiques ; outil shiny ;
Langages : python, Bash, R, C++, allemand :)
Expérience Galaxy : coporteur de l'instance SIWAA, développement de tools pour la simulation et l'analyse de modèles dynamiques d'intérrêt agronomique.
Attente : mettre à disposition des outils shiny

## Dallet Romain 
Localisation : CNRS - Membre IFB
Fonction : animateur
Cours suivis : 
Profil : Ingénieur d'étude
Langages : R, Python
Expérience Galaxy : Anciennement développeur d'interactive tools du projet Workflow4Metabolomics. Actuellement travaillant sur la cloudification du projet Galaxy Genome Annotation.

## Giacomoni	Franck	
Localisation : INRAE Clermont Ferrand
Fonction : animateur
Cours suivis : Introduction sur les IT dans Galaxy, Développer des IT pour Galaxy (aka dev)
Profil : Bioinformatique, systèmes d'informations
Langages : Perl, python, latin :)
Expérience Galaxy : Membre du feu GT Galaxy France, coporteur de l'instance WorkFlow4Metabolomics, développeur d'outil en métabolomique.


## Goué Nadia
Localisation : Université Clermont Auvergne
Fonction : apprenant
Cours suivis : Développer des IT pour Galaxy (aka dev)	
Profil :
Lanagage : Bashn, Perl, Python, R	
Expérience Galaxy : Administration du serveur Galaxy de l'UCA, Développement d'outils
Attente : faire de la formation

## Guitton	Yann	
Localisation : ONIRIS UMR INRAE	
Cours suivis : Introduction sur les IT dans Galaxy, Mettre en place un serveur Galaxy avec IT (aka admin), Développer des IT pour Galaxy (aka dev)
Profil : 
Langages : R	
Expérience Galaxy :

## Laheurte	Anne	
Localisation  : INRAE	Narbonne
Cours suivis : Introduction sur les IT dans Galaxy, Mettre en place un serveur Galaxy avec IT (aka admin), Développer des IT pour Galaxy (aka dev)
Profil : dev 
Langages : python, R	
Expérience Galaxy : instance galaxy pour une application R sur la typologie des micropolluants 

## Latrille	Eric	
Localisation : INRAE Narbonne Laboratoire Biotechnologie de l'Environnement
Fonction : animateur
Cours suivis : Introduction sur les IT dans Galaxy, Mettre en place un serveur Galaxy avec IT (aka admin), Développer des IT pour Galaxy (aka dev)
Profil : 
Langages : R, php, python	
Expérience Galaxy : traitement de spectres infra-rouge ; chemflow permet de faire les exercices d'un MOOC ; scilab, octave, R. 

## Le Bras Yvan
Localisation : MNHN
Fonction : animateur
Cours suivis :
Profil :
Langages :
Expérience Galaxy : IT ecologie ; R-Shiny

## Lefebvre Marie
Localisation : INRAE Bordeaux
Cours suivis : Mettre en place un serveur Galaxy avec IT (aka admin), Développer des IT pour Galaxy (aka dev)
Profil :
Langages : python, js
Expérience Galaxy : galaxisation d'outils

## Lollier	Virginie	
Localisation : 	INRAE	
Cours suivis : Introduction sur les IT dans Galaxy	
Profil : plateforme BIP
Langages : Python
Expérience Galaxy : traitement de données ; mise à disposition de chercheurs ou de thésards pour des activité de recherche 
Attente : savoir si je dois m'investir sur les IT ; intéresser à participer à + mais ne peut pas suivre cette semaine

## Maman Sarah
Localisation : INRAE Toulouse
Profil : Ingénieur d'Etude sur la plateforme sigenae
Langages : Python, perl, Cheetah.
Expérience Galaxy : administration d’une instance locale, support utilisateurs, wrapping d’outils.

## Mareuil	Fabien	
Localisation : Institut Pasteur	
Travaille avec Remi
Cours suivis : Introduction sur les IT dans Galaxy, Mettre en place un serveur Galaxy avec IT (aka admin), Développer des IT pour Galaxy (aka dev)
Profil : Ingénieur de Recherche
Langages : Python
Expérience Galaxy : Administration du serveur Galaxy de l'Institut Pasteur ; dev web et logiciel ; acquérir des compétences pour IT pour intégrer des outils RShiny.

## Pavot Lain 
Localisation : 	INRAE	
Fonction : animateur
Cours suivis : 
Profil : dev web ; plateforme métabolomique W4M avec Franck ; 
Langages : 
Expérience Galaxy : ansible galaxy ; ré-écriture d'un outil en R et RShiny + dockeriser des outils. 
Attentes : intégrer les outils existantes 

## Pelletier Dominique
Localisation : 	ifremer	
Fonction : animateur
Cours suivis :
Profil :
Langages :
Expérience Galaxy : peu d'expéirnce ; dev d'une apopli d'analyse de données ; mise à disposition d'outils ;

## Planel	Rémi	
Localisation : Institut Pasteur	
Cours suivis : Introduction sur les IT dans Galaxy, Mettre en place un serveur Galaxy avec IT (aka admin), Développer des IT pour Galaxy (aka dev)
Profil : Bioinformaticien ; dev web ; Ingénieur de Recherche
Langages : Python, javascript
Expérience galaxy : Administration du serveur Galaxy de l'Institut Pasteur ; webook sur la classif d'images ; 
Attente : développer nos connaissances sur ansible ; développer des IT utilisant des jupyter notebooks

## Pétéra	Mélanie	
Localisation : 	INRAE	
Cours suivis : Développer des IT pour Galaxy (aka dev)	
Profil : ouvrier des stats
Langages : R	
Expérience Galaxy : accointance avec Workflow4Metabolomics, utilisateur et développement d'outils pour le traitement de données métabolomiques

## Rossard Virginie ![](https://i.imgur.com/4iXIThh.jpg)

Localisation : INRAE Narbonne
Fonction : animatrice
Cours suivis : Introduction sur les IT dans Galaxy, Mettre en place un serveur Galaxy avec IT (aka admin), Développer des IT pour Galaxy (aka dev)
Profil : 
Langages : R, PHP, bash
Expérience Galaxy : administration d'un serveur d'une centaine d'utilisateurs

## Royaux Coline 
Localisation : MNHN
Fonction : animatrice
Cours suivis :
Profil :
Langages :
Expérience Galaxy : outils galaxy pour analyse de données en écologie
Attente : apprendre + sur les IT

## Tournayre	Jérémy	
Localisation : INRAE	
Cours suivis : Introduction sur les IT dans Galaxy, Mettre en place un serveur Galaxy avec IT (aka admin), Développer des IT pour Galaxy (aka dev)
Profil : IE dev Theix ; autour de données omics ; mise en place de pipelines. 
Langages : Perl, R, HTML, PHP, JS, Python	
Expérience Galaxy : 
Attente : mise en place d'IT


## 9 decembre 20

- introduction, présentation des participants 

9h-13h avec pause dans la matinée 


- Présentation de Y.Le Bras 

aller + loin avec les IT
les plus utilises: jupyter notebook, Rstudio, applis shiny
plus cytoscape, annotation d'images etc
Galaxy Iteractive Tools (et non plus Environments)
les IT sont des OUTILS comme tous les autres, jaunes quand le job tourne->différence: là où on peut utiliser l'outil, dans un menu "active interactive tools" 
on ouvre un nouvel onglet:  l'IT doit aller chercher les fichiers dans Galaxy 
l'URL est partageable avec d'autres utilisateurs / même user 
l'IT peut être mis dans un workflow
les IT passent par des containers: docker OK, singularity peut-être 

1° IT = askomics

- Lain Pavot





