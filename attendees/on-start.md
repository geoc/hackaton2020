# Intro
~~Le lien vers un dépôt/pad/hackmd avec toutes les infos sur les ressources (tutos, VM), horaires, lien visio, chat etc.~~  
Couper vos micros quand vous ne parlez pas.

# Début de la session admin
Prendre le temps de se connecter à la VM  : donner le lien pour les VMs + les instructions.  
~~Sur les VMs, ils ont installé byobu. Tu as des onglets : pour en créer un tu fais F2 et ensuite pour passer de l'un à l'autre F3 et F4 et quand tu veux en fermer un du fais un exit ou ctrl-D comme d'hab.  Pour voir le détail d'une réponse dans le terminal F7 puis navigation avec les flèches up et down. Pour sortir Ctrl + C.~~  
Comment télécharger une vidéo.  
Comment revenir dans la salle principale de BBB après êêtre allé dans une salle privée.  
VM petite : supprimer l'image téléchargée pour en mettre un nouvelle.   
~~Attention, ceux qui ne font que l'intro ou qui ont voulu être auditeur libre, ne vous attribuez pas de VM, nous avons juste ce qu'il faut.~~

# Début de la session dev 
Prendre le temps de se connecter à la VM  : donner le lien pour les VMs + les instructions.  
Présenter diapo pour ajouter un IT dans Galaxy via Ansible + commandes docker pour gérer les images `docker rm`, `docker images` . Attention, au préalable sudo -iu galaxy car c'est l'utilisateur qui est dans le grope docker.
Donner la localisation finale des fichiers job_conf.xml, galaxy.xml, wrappers.  
VM petite : supprimer l'image téléchargée pour en mettre un nouvelle.

