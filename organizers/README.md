# Notes à destination des organisateurs

## Définition et attribution des rôles

**Animateur** : anime la session  
**Assistant** : assure une aide technique aux participants, éventuellement à l'appel de l'alerteur  
**Alerteur** : lit les questions/commentaires dans le chat et les énonce à voix haute si nécessaire, vérifie les mails d'éventuels participants bloqués en dehors de la visio, demande aux assitants de venir en aide aux participants  
**Maître du temps** : alerte en fin de créneau, coupe les débats qui durent et n'apportent pas à l'objet du hack  
**Secrétaire** : s'assurer de la bonne organisation des notes  

| Session | Animateurs | Assitants | Alerteur | Maitre du temps et enregistrement | Scribe | Absents |
| ------- | ---------- | --------- | -------- | --------------- | ------ | ------- |
| Intro - 7/12 | Patrick & Yvan | Anthony & Lain | Franck | Éric | tous + Estelle | --- |
| Admin - 8/12 | Anthony & Estelle | Virginie & Lain | Coline | Patrick | tous + Patrick | Elie, Jean-Claude, Franck |
| Dev - 9/12 | Franck, Yvan, Romain, Lain, Coline, Élie, Anthony | Idem | Estelle | Virginie | tous + Jean-Claude | --- |
| Dev - 10/12 | Idem | Idem | Patrick | Virginie | tous + Eric | Estelle |


## 30 mn avant le début d'une session

https://inrae-bbb.learnatech.com/b/vir-ibp-shp-guo  
Chacun se connecte afin d'être nomé modérateur par ceux qui ont des comptes BBB (Virginie, Franck, Estelle). Ceci vous permettra de pouvoir partager votre écran, de créer des réunions privées, des sondages etc.  

Il faudrait créer 2 sous-salles à ce moment-là pour que des personnes puissent s'isoler en cas de problème.

## Prise de notes

Cela se fait ici : [hackmd](https://hackmd.io/yHhSNbIaS4WBFSRG3h-DZg?view).  

## Participation aux journées

Lundi 7/12/2020 après-midi : 22 utilisateurs connectés

Mardi 8/12/2020 matin : 

Mercredi 09/12/2020 matin : 24 utilisateurs connectés

Jeudi 10/12/2020 matin : 18 utilisateurs connectés


